<?php

namespace Drupal\kiyoh_rating;

/**
 * Helper class which contains the supported languages.
 */
class KiyohSupportedLanguages {

  /**
   * Predefined languages supported by Kiyoh.
   */
  public const LANG_CODES = [
    'en',
    'nl',
    'fi-FI',
    'fr',
    'be',
    'de',
    'hu',
    'bg',
    'ro',
    'hr',
    'ja',
    'es-ES',
    'it',
    'pt-PT',
    'tr',
    'nn-NL',
    'sv-SE',
    'da',
    'pt-BR',
    'pl',
    'sl',
    'zh-CN',
    'ru',
    'el',
    'cs',
    'et',
    'lt',
    'lv',
    'sk',
  ];

}
