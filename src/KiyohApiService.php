<?php

namespace Drupal\kiyoh_rating;

use Drupal\Core\KeyValueStore\KeyValueExpirableFactory;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Language\LanguageManagerInterface;
use JKetelaar\Kiyoh\Kiyoh;

/**
 * The Kiyoh api service.
 *
 * @package Drupal\kiyoh_rating
 */
class KiyohApiService {

  /**
   * Key value store to store the values from kiyoh in.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  private KeyValueStoreExpirableInterface $keyValueStore;

  /**
   * LanguageManager to get the current language.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private LanguageManagerInterface $languageManager;

  /**
   * Constant to define the module name.
   */
  private const KIYOH_RATING_MODULE_NAME = 'kiyoh_rating';

  /**
   * Constant to save rating values with the correct key.
   */
  private const KIYOH_COMPANY_RATING_KEY = 'kiyoh_company_rating_';

  /**
   * Constant to save location name values with the correct key.
   */
  private const KIYOH_COMPANY_LOCATION_NAME_KEY = 'kiyoh_company_location_name_';

  /**
   * Constant to save location id values with the correct key.
   */
  private const KIYOH_COMPANY_LOCATION_ID_KEY = 'kiyoh_company_location_id_';

  /**
   * Constant to save amount of rating values with the correct key.
   */
  private const KIYOH_AMOUNT_OF_RATINGS_KEY = 'kiyoh_amount_ratings_';

  /**
   * Expiry time of a set value.
   */
  private const KEY_VALUE_EXPIRE_TIME = 86400;

  /**
   * The url where reviews are published.
   */
  private const KIYOH_REVIEW_URL = 'https://www.kiyoh.com/reviews/';

  /**
   * The url where reviews can be added.
   */
  private const KIYOH_ADD_REVIEW_URL = 'https://www.kiyoh.com/add-review/';

  /**
   * The out of total for the ratings.
   */
  private const KIYOH_TOTAL_RATING = 10;

  /**
   * Array of instances of Kiyoh to use.
   *
   * @var array
   */
  private array $kiyohInstances = [];

  /**
   * KiyohApiService constructor.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactory $keyValueExpirableFactory
   *   KeyValueFactory to get a module specific key value store.
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   *   LanguageManager to get the current language later on.
   */
  public function __construct(KeyValueExpirableFactory $keyValueExpirableFactory, LanguageManager $languageManager) {
    $this->keyValueStore = $keyValueExpirableFactory->get(self::KIYOH_RATING_MODULE_NAME);
    $this->languageManager = $languageManager;
  }

  /**
   * Returns the current rating from the last 12 months.
   *
   * @param string $hash
   *   Hash to use.
   * @param int $reviewCount
   *   Amount of reviews to get.
   *
   * @return float
   *   The rating.
   */
  public function getRating(string $hash, int $reviewCount): float {
    $rating = $this->retrieveFromKeyValueStore(self::KIYOH_COMPANY_RATING_KEY . $hash);

    if (!empty($rating)) {
      return $rating;
    }

    $kiyoh = $this->getKiyoh($hash, $reviewCount);
    $rating = $kiyoh->getCompany()->getLast12MonthAverageRating();

    return $this->setInKeyValueStore(self::KIYOH_COMPANY_RATING_KEY . $hash, $rating);
  }

  /**
   * Gets the link to the Kiyoh review page of the current company.
   *
   * @param string $hash
   *   Hash to use.
   * @param int $reviewCount
   *   Amount of reviews to get.
   *
   * @return string
   *   String containing the URL.
   */
  public function getCompanyUrl(string $hash, int $reviewCount): string {
    return self::KIYOH_REVIEW_URL . $this->getBaseUrl($hash, $reviewCount);
  }

  /**
   * Gets the link to the add a review to the current company.
   *
   * @param string $hash
   *   Hash to use.
   * @param int $reviewCount
   *   Amount of reviews to get.
   *
   * @return string
   *   String containing the URL.
   */
  public function getAddReviewUrl(string $hash, int $reviewCount): string {
    return self::KIYOH_ADD_REVIEW_URL . $this->getBaseUrl($hash, $reviewCount);
  }

  /**
   * Gets the link to the Kiyoh base url.
   *
   * @param string $hash
   *   Hash to use.
   * @param int $reviewCount
   *   Amount of reviews to get.
   *
   * @return string
   *   String containing the URL.
   */
  private function getBaseUrl(string $hash, int $reviewCount): string {
    $companyLocationId = $this->retrieveFromKeyValueStore(self::KIYOH_COMPANY_LOCATION_ID_KEY . $hash);
    $companyLocationName = $this->retrieveFromKeyValueStore(self::KIYOH_COMPANY_LOCATION_NAME_KEY . $hash);

    if (empty($companyLocationId)) {
      $companyLocationId = $this->getKiyoh($hash, $reviewCount)->getCompany()->getLocationId();
      $this->setInKeyValueStore(self::KIYOH_COMPANY_LOCATION_ID_KEY . $hash, $companyLocationId);
    }
    if (empty($companyLocationName)) {
      $companyLocationName = $this->getKiyoh($hash, $reviewCount)->getCompany()->getLocationName();
      $this->setInKeyValueStore(self::KIYOH_COMPANY_LOCATION_NAME_KEY . $hash, $companyLocationName);
    }

    $lang = $this->getLanguage();
    return $companyLocationId . '/' . strtolower(str_replace('.', '_', $companyLocationName)) . '?lang=' . $lang;
  }

  /**
   * Gets the current language with country code.
   *
   * E.g. nl-NL, nl-BE, pt-PT.
   *
   * @return string
   *   language and country code.
   */
  private function getLanguage() : string {
    $langCode = strtolower($this->languageManager->getCurrentLanguage()->getId());
    $lowercaseKiyohLanguages = array_map('strtolower', KiyohSupportedLanguages::LANG_CODES);
    $keyedKiyohLanguages = array_combine($lowercaseKiyohLanguages, KiyohSupportedLanguages::LANG_CODES);

    if (in_array($langCode, $lowercaseKiyohLanguages, TRUE)) {
      return $keyedKiyohLanguages[$langCode];
    }

    $langCodeSegments = explode('-', $langCode);
    if (in_array($langCodeSegments[0], $lowercaseKiyohLanguages, TRUE)) {
      return $keyedKiyohLanguages[$langCodeSegments[0]];
    }

    foreach ($lowercaseKiyohLanguages as $kiyohLangcode) {
      $kiyohLangSements = explode('-', $kiyohLangcode);
      if (count($kiyohLangSements) < 2) {
        continue;
      }

      if ($kiyohLangSements[0] === $langCodeSegments[0]) {
        return $keyedKiyohLanguages[$kiyohLangcode];
      }
    }

    return 'en';
  }

  /**
   * Gets the total amount of reviews for your company.
   *
   * @param string $hash
   *   Hash to use.
   * @param int $reviewCount
   *   Amount of reviews to get.
   *
   * @return int
   *   Amount of total reviews.
   */
  public function getAmountOfReviews(string $hash, int $reviewCount): int {
    $amountOfReviews = $this->retrieveFromKeyValueStore(self::KIYOH_AMOUNT_OF_RATINGS_KEY . $hash);

    if (!empty($amountOfReviews)) {
      return $amountOfReviews;
    }

    $kiyoh = $this->getKiyoh($hash, $reviewCount);
    $amountOfReviews = $kiyoh->getCompany()->getNumberReviews();
    return $this->setInKeyValueStore(self::KIYOH_AMOUNT_OF_RATINGS_KEY . $hash, $amountOfReviews);
  }

  /**
   * Retrieve a value from the key value store.
   *
   * @param string $key
   *   The key to use.
   *
   * @return string|null
   *   String if found, otherwise null.
   */
  private function retrieveFromKeyValueStore(string $key) {
    return $this->keyValueStore->get($key);
  }

  /**
   * Get a kiyoh instance.
   *
   * @param string $hash
   *   Hash to use.
   * @param int $reviewCount
   *   Amount of reviews to get.
   *
   * @return \JKetelaar\Kiyoh\Kiyoh
   *   Kiyoh instance.
   */
  public function getKiyoh(string $hash, int $reviewCount = 10): Kiyoh {
    if (!isset($this->kiyohInstances[$hash][$reviewCount])) {
      $this->kiyohInstances[$hash][$reviewCount] = new Kiyoh($hash, $reviewCount);
    }
    return $this->kiyohInstances[$hash][$reviewCount];
  }

  /**
   * Set a value in the key store and return the set value.
   *
   * @param string $key
   *   Key to use.
   * @param string $value
   *   Value to set.
   *
   * @return string
   *   THE Sset value.
   */
  private function setInKeyValueStore(string $key, string $value): string {
    $this->keyValueStore->setWithExpire($key, $value, self::KEY_VALUE_EXPIRE_TIME);
    return $value;
  }

  /**
   * Returns the total rating.
   *
   * @return int
   *   The total rating.
   */
  public function getTotalRating() {
    return self::KIYOH_TOTAL_RATING;
  }

}
