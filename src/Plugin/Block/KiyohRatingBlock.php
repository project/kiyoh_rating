<?php

namespace Drupal\kiyoh_rating\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Block for the Kiyoh ratings.
 *
 * @Block(
 *   id = "kiyoh_rating_block",
 *   admin_label = @Translation("Kiyoh rating"),
 *   category = @Translation("Kiyoh rating")
 * )
 */
class KiyohRatingBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $config = $this->getConfiguration();

    return [
      '#theme' => 'kiyoh_rating_big',
      '#kiyoh_hash' => $config['kiyoh_hash'] ?? '',
      '#override_kiyoh_company_url' => $config['override_kiyoh_company_url'] ?? '',
      '#override_kiyoh_rate_us_url' => $config['override_kiyoh_rate_us_url'] ?? '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['kiyoh_hash'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Kiyoh hash'),
      '#description' => $this->t('Fill in your kiyoh hash from the XML API'),
      '#default_value' => $config['kiyoh_hash'] ?? '',
      '#size' => 16,
      '#maxlength' => 16,
      '#required' => TRUE,
    ];

    $form['overrides'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#description' => $this->t('These settings override certain results from obtained via the api. <br/>For example when your multi-site has reviews on x domains but not this one.'),
    ];

    $form['overrides']['override_kiyoh_company_url'] = [
      '#type' => 'textfield',
      '#description' => $this->t('The company url override.'),
      '#default_value' => $config['override_kiyoh_company_url'] ?? '',
    ];

    $form['overrides']['override_kiyoh_rate_us_url'] = [
      '#type' => 'textfield',
      '#description' => $this->t('The url for rating.'),
      '#default_value' => $config['override_kiyoh_rate_us_url'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['kiyoh_hash'] = $form_state->getValue('kiyoh_hash');
    $overrides = $form_state->getValue('overrides');
    $this->configuration['override_kiyoh_company_url'] = $overrides['override_kiyoh_company_url'];
    $this->configuration['override_kiyoh_rate_us_url'] = $overrides['override_kiyoh_rate_us_url'];
    parent::blockSubmit($form, $form_state);
  }

}
